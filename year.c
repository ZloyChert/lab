#include <stdio.h>
#include <time.h>
#include <memory.h>

int main()
{
	struct tm infotime;
	time_t loc = 0,bday = 0;
	double age = 0;
	

	memset(&infotime, 0, sizeof infotime); //инициализируем инфотайм нулям
	time(&loc);	
	printf("Enter birthday dd.mm.yyyy hh:mm\n ");
	scanf("%d.%d.%d %d:%d", &infotime.tm_mday, &infotime.tm_mon, &infotime.tm_year, &infotime.tm_hour, &infotime.tm_min);
	infotime.tm_mon -= 1;
	infotime.tm_year -= 1900;
	
	
	bday = mktime(&infotime);
	age = difftime(loc, bday);

	printf("You are %f seconds old.\n", age);
return 0;
}
